Самое главное на сайте это абонименты и аренда\продажа оборудования, по этому они будут на главном сайте.
Бар и блог почти не связаны с главным сайтом, поэтому это будут как отдельные проекты.

WebSocket нужно использовать везде где идет покупка, оренда что бы все пользователи видели актуальную информацию.

Для блога можно использовать монолитную архитектуру, поскольку там не ожидается каких-то колосальных изменений.

Для бара и основной части бизнеса лучше использовать микросервисную архитекртуру, это позволит легче
масштрабировать бизнес в будующем, легче добавлять новый и удалять старый функционал, а так же 
разгрузит основной сервер.
